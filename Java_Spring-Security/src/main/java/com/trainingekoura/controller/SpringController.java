package com.trainingekoura.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController //renvoi au webService (JSON)
@RequestMapping("/user")
public class SpringController {

	@GetMapping
	@Secured({"ROLE_ADMIN"})
	public ResponseEntity<User> getUser(){
		User u = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return new ResponseEntity<User>(u, HttpStatus.OK);
	}
	
	@GetMapping
	@RequestMapping("/message")
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	public String getMessage(){
		String message = "Besoin de chocolat";
		return message;
	}
	
	
	
	
	
	public SpringController() {
		// TODO Auto-generated constructor stub
	}

}
