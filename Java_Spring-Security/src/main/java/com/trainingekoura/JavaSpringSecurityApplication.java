package com.trainingekoura;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.trainingekoura.core.model.User;
import com.trainingekoura.repo.UserRepository;

@SpringBootApplication
public class JavaSpringSecurityApplication implements CommandLineRunner{

	@Autowired
	private UserRepository userRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(JavaSpringSecurityApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		User user = new User();
		user.setUsername("nat");
		user.setRole("ADMIN");
		user.setPassword("nat");
		
		User user2 = new User();
		user2.setUsername("nat2");
		user2.setRole("USER");
		user2.setPassword("nat2");
		
		User user3 = new User();
		user3.setUsername("nat3");
		user3.setRole("DEV");
		user3.setPassword("nat3");
		
		userRepository.save(user);
		userRepository.save(user2);
		userRepository.save(user3);
		
	}

}
