package com.trainingekoura.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.trainingekoura.core.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

	@Query(" select u from User u where u.username = ?1")
	   Optional<User> findUserWithName(String username);
	
	
	
	
	
	
}
