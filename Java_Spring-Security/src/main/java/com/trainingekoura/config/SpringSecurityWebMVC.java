package com.trainingekoura.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.trainingekoura.service.UserService;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
		prePostEnabled = true,
		securedEnabled = true,
		jsr250Enabled = true)
public class SpringSecurityWebMVC extends WebSecurityConfigurerAdapter{
	
	@Autowired
	UserService userDetailsService;
	
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//		auth.inMemoryAuthentication()
//		.withUser("dev").password("{noop}dev").roles("dev")
//			.and()
//		.withUser("admin").password("{noop}admin").roles("admin");
		//implique que l'on doit entrer le user en dur
		
		
		auth.userDetailsService(userDetailsService);
		//implique que l'on souhaite fait appel à des utilisateurs présents dans notre base de donnée
		
	}
	
	
	@Override
	protected void configure (HttpSecurity http) throws Exception
	{
		
		//Quand on entre les données en dur
//		http
//		.authorizeRequests()
//			.anyRequest()
//			.fullyAuthenticated()
//			.and()
//		.httpBasic();
//		
//		http.csrf().disable();
		
		//Quand on fait appel à la BDD
		http
        .authorizeRequests()
            .antMatchers("/login", "/security/login", "/security/logout", "/users/create")
            .permitAll()
            .and()
        .authorizeRequests()
            .anyRequest()
            .authenticated()
            .and()
        .formLogin()
            .loginPage("/login")
            .permitAll()
            .and()
        .logout()
            .invalidateHttpSession(true)
            .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
            .logoutSuccessUrl("/login")
            .and()
		.authorizeRequests()
			.antMatchers("/user/**")
			.hasRole("ADMIN")
		
			.and()
			
			
		.authorizeRequests()
			.antMatchers("/user/message")
			.hasRole("USER");
		

		
		
	}
	

}
