package com.trainingekoura.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.trainingekoura.core.model.User;
import com.trainingekoura.repo.UserRepository;

@Service
public class UserService implements UserDetailsService{

	public final UserRepository userRepository;

	@Autowired
	public UserService(UserRepository userRepository)
	{
		this.userRepository = userRepository;
	}
	
	
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		Objects.requireNonNull(username);
		
		BCryptPasswordEncoder encoder = passwordEncoder();
		
		User user = userRepository.findUserWithName(username)
				.orElseThrow(()-> new UsernameNotFoundException("User not found"));
		
		return new org.springframework.security.core.userdetails.
				User(user.getUsername(),encoder.encode(user.getPassword()),getGrantedAuthorities(user));
	}
	
	private Collection<GrantedAuthority> getGrantedAuthorities(User user){
		 Collection<GrantedAuthority> grantedAuthority = new ArrayList<>();
	       System.out.println( "Le role ==> " + user.getRole());
	       //TODO: Gérer ce cas .
	       if(user.getRole() != null)
	    	   
	            if(user.getRole().equals("ADMIN")) {
	                grantedAuthority.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
	            }
	       	
	       		if(user.getRole().equals("USER"))
	       		{
	       			System.out.println("message");
	       			grantedAuthority.add(new SimpleGrantedAuthority("ROLE_USER"));
	       		}
	       		
	       		
	       grantedAuthority.add(new SimpleGrantedAuthority("ROLE_DEV"));

	       return grantedAuthority;
	}



	@Bean
	private BCryptPasswordEncoder passwordEncoder() {
		// TODO Auto-generated method stub
		return new BCryptPasswordEncoder();
	}
	//Permet de crypter le mot de passe

}
